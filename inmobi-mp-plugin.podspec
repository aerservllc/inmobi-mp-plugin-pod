### inmobi-mp-plugin.podspec

Pod::Spec.new do |s|
  s.name                  = 'inmobi-mp-plugin'
  s.version               = '1.0.1.820'
  s.summary               = 'InMobi Audience Bidder - MoPub Support'
  s.description           = <<-DESC
InMobi Audience Bidder - MoPub Support. Copyright 2019 InMobi, all rights reserved.
                            DESC
  s.homepage              = 'https://bitbucket.org/aerservllc/inmobi-mp-plugin-pod'
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { 'albzhu' => 'albert.zhu@inmobi.com' }
  s.source                = { :git => 'https://bitbucket.org/aerservllc/inmobi-mp-plugin-pod.git', :tag => s.version.to_s }
  s.platform              = :ios, '9.0'
  s.ios.deployment_target = '9.0'
  s.default_subspecs      = 'InMobiABMoPubPlugin'

  s.subspec 'InMobiSDK' do |imss|
    imss.dependency            'mopub-ios-sdk' 
    imss.source_files          = 'inmobi-mp-plugin/Frameworks/InMobiSDK.framework/Headers/*.{h,m}'
    imss.public_header_files   = 'inmobi-mp-plugin/Frameworks/InMobiSDK.framework/Headers/*.h'
    imss.vendored_frameworks   = 'inmobi-mp-plugin/Frameworks/InMobiSDK.framework'
    imss.preserve_paths        = 'inmobi-mp-plugin/Frameworks/InMobiSDK.framework'
  end

  s.subspec 'InMobiABMoPubPlugin' do |imabmpss|
    imabmpss.dependency            'mopub-ios-sdk' 
    imabmpss.dependency            'inmobi-mp-plugin/InMobiSDK'
    imabmpss.source_files          = 'inmobi-mp-plugin/Frameworks/InMobiABMoPubPlugin.framework/Headers/*.{h,m}'
    imabmpss.public_header_files   = 'inmobi-mp-plugin/Frameworks/InMobiABMoPubPlugin.framework/Headers/*.h'
    imabmpss.vendored_frameworks   = 'inmobi-mp-plugin/Frameworks/InMobiABMoPubPlugin.framework'
    imabmpss.preserve_paths        = 'inmobi-mp-plugin/Frameworks/InMobiABMoPubPlugin.framework'
  end  
end
