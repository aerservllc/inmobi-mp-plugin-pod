# inmobi-mp-plugin

[![CI Status](https://img.shields.io/travis/albzhu/inmobi-mp-plugin.svg?style=flat)](https://travis-ci.org/albzhu/inmobi-mp-plugin)
[![Version](https://img.shields.io/cocoapods/v/inmobi-mp-plugin.svg?style=flat)](https://cocoapods.org/pods/inmobi-mp-plugin)
[![License](https://img.shields.io/cocoapods/l/inmobi-mp-plugin.svg?style=flat)](https://cocoapods.org/pods/inmobi-mp-plugin)
[![Platform](https://img.shields.io/cocoapods/p/inmobi-mp-plugin.svg?style=flat)](https://cocoapods.org/pods/inmobi-mp-plugin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

inmobi-mp-plugin is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'inmobi-mp-plugin', :git => 'https://bitbucket.org/aerservllc/inmobi-mp-plugin-pod.git'
```

## Author

albzhu, albert.zhu@inmobi.com

## License

inmobi-mp-plugin is available under the MIT license. See the LICENSE file for more info.
